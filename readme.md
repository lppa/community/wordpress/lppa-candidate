Instructions

Install WordPress on your hosting service of choice
Install “GeneratePress” theme (this will be your “parent” theme)
Install “LPPA Candidate” child theme, and activate it.

Notes:

Customize your child theme files here:
/public_html/wp-content/themes/lppa-candidate/

Parent theme should be here:
/public_html/wp-content/themes/generatepress/
